@echo off

rem ################################
rem # check command line arguments #
rem ################################
:checkargs
if "%1" == "" goto usage

rem ######################
rem # test for JAVA_HOME #
rem ######################
set JAVA=java
set JAVAC=javac
set JAR=jar
if "%JAVA_HOME%" == "" goto nojava

set SRC_PKG=HalloweenTime
rem # we must have JAVA_HOME set
set JAVA="%JAVA_HOME%\bin\java"
set JAVAC="%JAVA_HOME%\bin\javac"
set JAR="%JAVA_HOME%\bin\jar"
set RTI_HOME=%CD%
goto run

:nojava
echo ERROR Your JAVA_HOME environment variable is not set!
goto run

:run
if "%1" == "clean" goto clean
if "%1" == "compile" goto compile
if "%1" == "start" goto start
if "%1" == "play" goto play

rem ############################################
rem ### (target) clean #########################	
rem ############################################
:clean
echo "deleting federate jar file and left over logs"
del java-%SRC_PKG%.jar
rd /S /Q logs
goto finish

rem ############################################
rem ### (target) compile #######################
rem ############################################
:compile
echo "compiling HelloweenTime federate"
del java-%SRC_PKG%.jar
cd src
%JAVAC% -cp ".;%RTI_HOME%\portico.jar" %SRC_PKG%\Entity\*.java %SRC_PKG%\Environment\*.java %SRC_PKG%\Game\*.java %SRC_PKG%\Player\*.java Icons\Helper\*.java
%JAR% -cf ..\java-%SRC_PKG%.jar %SRC_PKG%\Entity\*.class %SRC_PKG%\Environment\*.class %SRC_PKG%\Game\*.class %SRC_PKG%\Player\*.class Icons\*.* Icons\Helper\*.class
cd..
goto finish

rem ############################################
rem ### (target) execute #######################
rem ############################################
:start
SHIFT
%JAVA% -cp "java-%SRC_PKG%.jar;%RTI_HOME%\portico.jar" %SRC_PKG%.Environment.EnvironmentFederate %1 %2 %3 %4 %5 %6 %7 %8 %9
goto finish

:play
SHIFT
%JAVA% -cp "java-%SRC_PKG%.jar;%RTI_HOME%\portico.jar" %SRC_PKG%.Player.PlayerFederate %1 %2 %3 %4 %5 %6 %7 %8 %9
goto finish

:usage
echo usage: game.bat [compile] [clean] [start] [play]
echo game.bat start
echo game.bat play
:finish

