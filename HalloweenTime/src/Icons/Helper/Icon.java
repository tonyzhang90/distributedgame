package Icons.Helper;

import java.awt.Graphics;
import java.awt.Image;

public class Icon {
    private final Image image;

    public Icon(Image image) {
            this.image = image;
    }

    public int getWidth() {
            return image.getWidth(null);
    }

    public int getHeight() {
            return image.getHeight(null);
    }

    // Draw the icon onto the graphics context provided
    public void draw(Graphics g,int x,int y) {
        g.drawImage(image, x-image.getWidth(null)/2, y-image.getHeight(null)/2, null);
    }
}