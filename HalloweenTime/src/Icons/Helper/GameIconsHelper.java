package Icons.Helper;

import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import javax.imageio.ImageIO;

public class GameIconsHelper {
    // Singleton class instance
    private static final GameIconsHelper singleton = new GameIconsHelper(); 
	
    // Return singleton class instance
    public static GameIconsHelper get() {
        return singleton;
    }
    // HashMap to store all icon with their file paths
    private final HashMap icons = new HashMap();
	
    /**
     * @param iconpath
     * @return
     */
    public Icon getIcon(String iconpath)
    {
        if (icons.get(iconpath) != null) {
            return (Icon) icons.get(iconpath);
        }
        //load icon
        BufferedImage sourceImage = null;
        try
        {
            URL url = this.getClass().getClassLoader().getResource(iconpath);
            if (url ==null) {
                    fail("Can't find icon path: " + iconpath);
            }
            sourceImage = ImageIO.read(url);
        }
        catch (IOException e) {
            fail("Failed to load " + iconpath);
        }
        //create an accelerated image of the right size from loaded image
        GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        Image image = gc.createCompatibleImage(sourceImage.getWidth(), sourceImage.getHeight(), Transparency.BITMASK);
        image.getGraphics().drawImage(sourceImage, 0, 0, null);
        //create a icon and add it to the cache
        Icon icon = new Icon(image);
        icons.put(iconpath, icon);

        return icon;
    }

    private void fail(String message) {
        System.err.println("message: " + message);
        System.exit(0);
    }
}
