/*
 *   Copyright 2007 The Portico Project
 *
 *   This file is part of portico.
 *
 *   portico is free software; you can redistribute it and/or modify
 *   it under the terms of the Common Developer and Distribution License (CDDL) 
 *   as published by Sun Microsystems. For more information see the LICENSE file.
 *   
 *   Use of this software is strictly AT YOUR OWN RISK!!!
 *   If something bad happens you do not have permission to come crying to me.
 *   (that goes for your lawyer as well)
 *
 */
package HalloweenTime.Environment;

import hla.rti.EventRetractionHandle;
import hla.rti.LogicalTime;
import hla.rti.RTIexception;
import hla.rti.ReflectedAttributes;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.NullFederateAmbassador;
import java.util.StringTokenizer;

import org.portico.impl.hla13.types.DoubleTime;

/**
 * This class handles all incoming callbacks from the RTI regarding a particular
 * {@link EnvironmentFederate}. It will log information about any callbacks it
 * receives, thus demonstrating how to deal with the provided callback information.
 */
public class EnvironmentFederateAmbassador extends NullFederateAmbassador
{
    //----------------------------------------------------------
    //                    STATIC VARIABLES
    //----------------------------------------------------------

    //----------------------------------------------------------
    //                   INSTANCE VARIABLES
    //----------------------------------------------------------
    // these variables are accessible in the package
    protected double federateTime        = 0.0;
    protected double federateLookahead   = 1.0;

    protected boolean isRegulating       = false;
    protected boolean isConstrained      = false;
    protected boolean isAdvancing        = false;

    protected boolean isAnnounced        = false;
    protected boolean isReadyToRun       = false;

    protected EnvironmentFederate Federate;

    //----------------------------------------------------------
    //                      CONSTRUCTORS
    //----------------------------------------------------------

    public EnvironmentFederateAmbassador(EnvironmentFederate Federate)
    {
        this.Federate = Federate;
    }

    //----------------------------------------------------------
    //                    INSTANCE METHODS
    //----------------------------------------------------------
    /**
     * As all time-related code is Portico-specific, we have isolated it into a single
     * method. This way, if you need to move to a different RTI, you only need to
     * change this code, rather than more code throughout the whole class.
     */
    private double convertTime( LogicalTime logicalTime )
    {
        // PORTICO SPECIFIC!!
        return ((DoubleTime)logicalTime).getTime();
    }

    private void log( String message )
    {
        System.out.println( "FederateAmbassador: " + message );
    }

    //////////////////////////////////////////////////////////////////////////
    ////////////////////////// RTI Callback Methods //////////////////////////
    //////////////////////////////////////////////////////////////////////////
    @Override
    public void synchronizationPointRegistrationFailed( String label )
    {
        log( "Failed to register sync point: " + label );
    }

    @Override
    public void synchronizationPointRegistrationSucceeded( String label )
    {
        log( "Successfully registered sync point: " + label );
    }

    @Override
    public void announceSynchronizationPoint( String label, byte[] tag )
    {
        log( "Synchronization point announced: " + label );
        if( label.equals(EnvironmentFederate.READY_TO_RUN) )
                this.isAnnounced = true;
    }

    @Override
    public void federationSynchronized( String label )
    {
        log( "Federation Synchronized: " + label );
        if( label.equals(EnvironmentFederate.READY_TO_RUN) )
                this.isReadyToRun = true;
    }

    /**
     * The RTI has informed us that time regulation is now enabled.
     */
    @Override
    public void timeRegulationEnabled( LogicalTime theFederateTime )
    {
        this.federateTime = convertTime( theFederateTime );
        this.isRegulating = true;
    }

    @Override
    public void timeConstrainedEnabled( LogicalTime theFederateTime )
    {
        this.federateTime = convertTime( theFederateTime );
        this.isConstrained = true;
    }

    @Override
    public void timeAdvanceGrant( LogicalTime theTime )
    {
        this.federateTime = convertTime( theTime );
        this.isAdvancing = false;
    }

    @Override
    public void discoverObjectInstance( int theObject,
                                        int theObjectClass,
                                        String objectName )
    {
        log( "Discoverd Object: handle=" + theObject + ", classHandle=" +
                 theObjectClass + ", name=" + objectName );
    }

    @Override
    public void reflectAttributeValues( int theObject,
                                        ReflectedAttributes theAttributes,
                                        byte[] tag )
    {
        // just pass it on to the other method for printing purposes
        // passing null as the time will let the other method know it
        // it from us, not from the RTI
        reflectAttributeValues( theObject, theAttributes, tag, null, null );
    }

    @Override
    public void reflectAttributeValues( int theObject,
                                        ReflectedAttributes theAttributes,
                                        byte[] tag,
                                        LogicalTime theTime,
                                        EventRetractionHandle retractionHandle )
    {
        try{
            int Class_Handle = Federate.rtiamb.getObjectClass(theObject);
            if(Class_Handle == Federate.avatar_handle){
                int x = Federate.rtiamb.getAttributeHandle( "x", Class_Handle);
                int y = Federate.rtiamb.getAttributeHandle( "y", Class_Handle);
                int candy_number = Federate.rtiamb.getAttributeHandle( "candy_number", Class_Handle);
                int entity_name = Federate.rtiamb.getAttributeHandle( "entity_name", Class_Handle);

                double avatar_x = 0.0; double avatar_y = 0.0;int avatar_candy_number = 0;
                String avatar_name = "";
                for(int i=0;i<theAttributes.size();i++)
                {
                    if(theAttributes.getAttributeHandle(i)==x)
                    {
                        avatar_x = EncodingHelpers.decodeDouble(theAttributes.getValue(i));
                    }
                    else if(theAttributes.getAttributeHandle(i)==y)
                    {
                        avatar_y = EncodingHelpers.decodeDouble(theAttributes.getValue(i));
                    }
                    else if(theAttributes.getAttributeHandle(i)==candy_number)
                    {
                        avatar_candy_number = EncodingHelpers.decodeInt(theAttributes.getValue(i));
                    }
                    else if(theAttributes.getAttributeHandle(i)==entity_name)
                    {
                        avatar_name = EncodingHelpers.decodeString(theAttributes.getValue(i));
                    }
                }   
                log("Receive update: " + avatar_name + " (" + avatar_x + "|" + avatar_y + "|" + avatar_candy_number + ")");
                StringTokenizer token = new StringTokenizer(avatar_name);
                token.nextToken();
                int id = Integer.parseInt(token.nextToken());

                // Only update the player avatar locations
                Federate.player_avatars[id].x = avatar_x;
                Federate.player_avatars[id].y = avatar_y;
                if (avatar_x < 0 && avatar_y < 0) 
                    Federate.online_players--;
            }
        }catch (RTIexception e){
            e.printStackTrace();
        }
    }

    @Override
    public void removeObjectInstance( int theObject, byte[] userSuppliedTag )
    {
        log( "Object Removed: handle=" + theObject );
    }

    @Override
    public void removeObjectInstance( int theObject,
                                      byte[] userSuppliedTag,
                                      LogicalTime theTime,
                                      EventRetractionHandle retractionHandle )
    {
        log( "Object Removed: handle=" + theObject );
    }
}
