package HalloweenTime.Environment;

import HalloweenTime.Entity.HouseEntity;
import HalloweenTime.Entity.AvatarEntity;
import HalloweenTime.Entity.Entity;
import static HalloweenTime.Game.SystemSettings.AUTO_AVATAR_IMG;
import static HalloweenTime.Game.SystemSettings.AVATAR_SENSE_RANGE;
import static HalloweenTime.Game.SystemSettings.AVATAR_SPEED;
import static HalloweenTime.Game.SystemSettings.AVATAR_VISION_RANGE;
import static HalloweenTime.Game.SystemSettings.FEDERATION_NAME;
import static HalloweenTime.Game.SystemSettings.HOUSE_IMG;
import static HalloweenTime.Game.SystemSettings.MAP_HEIGHT;
import static HalloweenTime.Game.SystemSettings.MAP_WIDTH;
import static HalloweenTime.Game.SystemSettings.PLAYER_AVATAR_IMG;
import static HalloweenTime.Game.SystemSettings.REPLENISH_AMOUNT;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.MalformedURLException;

import hla.rti.AttributeHandleSet;
import hla.rti.FederatesCurrentlyJoined;
import hla.rti.FederationExecutionAlreadyExists;
import hla.rti.FederationExecutionDoesNotExist;
import hla.rti.LogicalTime;
import hla.rti.LogicalTimeInterval;
import hla.rti.RTIambassador;
import hla.rti.RTIexception;
import hla.rti.ResignAction;
import hla.rti.SuppliedAttributes;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.RtiFactoryFactory;
import java.util.Random;
import java.util.Scanner;

import org.portico.impl.hla13.types.DoubleTime;
import org.portico.impl.hla13.types.DoubleTimeInterval;

public class EnvironmentFederate
{
    //----------------------------------------------------------
    //                    STATIC VARIABLES
    //----------------------------------------------------------

    /** The sync point all federates will sync up on before starting */
    public static final String READY_TO_RUN = "ReadyToRun";

    // Public variables
    public AvatarEntity[] player_avatars; // player avatar entities
    public int avatar_handle; // avatar value handle
    public int online_players; // online player numbers
    
    // private variables
    private AvatarEntity[] auto_avatars;  // auto avatar entities
    private HouseEntity[] houses; // house entities
    private int house_handle; // house value handle
    // user input for auto avatar number, house number and supported player numbers
    private static int auto_avatar_number, house_number, player_number;

    //----------------------------------------------------------
    //                   INSTANCE VARIABLES
    //----------------------------------------------------------
    RTIambassador rtiamb;
    private EnvironmentFederateAmbassador fedamb;

    //----------------------------------------------------------
    //                    INSTANCE METHODS
    //----------------------------------------------------------
    /**
     * This is just a helper method to make sure all logging it output in the same form
     */
    private void log( String message )
    {
        System.out.println( "EnvironmentFederate : " + message );
    }

    /**
     * This method will block until the user presses enter
     */
    private void waitForUser()
    {
        log( " >>>>>>>>>> Press Enter to Continue <<<<<<<<<<" );
        BufferedReader reader = new BufferedReader( new InputStreamReader(System.in) );
        try
        {
            reader.readLine();
        }
        catch( Exception e )
        {
            log( "Error while waiting for user input: " + e.getMessage() );
        }
    }

    /**
     * As all time-related code is Portico-specific, we have isolated it into a 
     * single method. This way, if you need to move to a different RTI, you only need
     * to change this code, rather than more code throughout the whole class.
     */
    private LogicalTime convertTime( double time )
    {
        // PORTICO SPECIFIC!!
        return new DoubleTime( time );
    }

    /**
     * Same as for {@link #convertTime(double)}
     */
    private LogicalTimeInterval convertInterval( double time )
    {
        // PORTICO SPECIFIC!!
        return new DoubleTimeInterval( time );
    }

    ///////////////////////////////////////////////////////////////////////////
    ////////////////////////// Main Simulation Method /////////////////////////
    ///////////////////////////////////////////////////////////////////////////

    public void runFederate( String federateName) throws RTIexception
    {
        /////////////////////////////////
        // 1. create the RTIambassador //
        /////////////////////////////////
        rtiamb = RtiFactoryFactory.getRtiFactory().createRtiAmbassador();
        // Set supported number of online players
        online_players = player_number;
        //////////////////////////////
        // 2. create the federation //
        //////////////////////////////
        // create
        // NOTE: some other federate may have already created the federation,
        //       in that case, we'll just try and join it
        try
        {
            File fom = new File( FEDERATION_NAME );
            rtiamb.createFederationExecution( "EnvironmentFederation",
                                              fom.toURI().toURL() );
            log( "Created Federation" );
        }
        catch( FederationExecutionAlreadyExists exists )
        {
            log( "Didn't create federation, it already existed" );
        }
        catch( MalformedURLException urle )
        {
            log( "Exception processing fom: " + urle.getMessage() );
            return;
        }

        ////////////////////////////
        // 3. join the federation //
        ////////////////////////////
        // create the federate ambassador and join the federation
        fedamb = new EnvironmentFederateAmbassador(this);
        rtiamb.joinFederationExecution( federateName, "EnvironmentFederation", fedamb );
        log( "Joined Federation as " + federateName );

        ////////////////////////////////
        // 4. announce the sync point //
        ////////////////////////////////
        // announce a sync point to get everyone on the same page. if the point
        // has already been registered, we'll get a callback saying it failed,
        // but we don't care about that, as long as someone registered it
        rtiamb.registerFederationSynchronizationPoint( READY_TO_RUN, null );

        // Initialize all objects
        Initialize();

        // wait until the point is announced
        while( fedamb.isAnnounced == false )
        {
            rtiamb.tick();
        }

        // WAIT FOR USER TO KICK US OFF
        // So that there is time to add other federates, we will wait until the
        // user hits enter before proceeding. That was, you have time to start
        // other federates.
        waitForUser();

        ///////////////////////////////////////////////////////
        // 5. achieve the point and wait for synchronization //
        ///////////////////////////////////////////////////////
        // tell the RTI we are ready to move past the sync point and then wait
        // until the federation has synchronized on
        rtiamb.synchronizationPointAchieved( READY_TO_RUN );
        log( "Achieved sync point: " +READY_TO_RUN+ ", waiting for federation..." );
        while( fedamb.isReadyToRun == false )
        {
            rtiamb.tick();
        }

        /////////////////////////////
        // 6. enable time policies //
        /////////////////////////////
        // in this section we enable/disable all time policies
        // note that this step is optional!
        enableTimePolicy();
        log( "Time Policy Enabled" );

        //////////////////////////////
        // 7. publish and subscribe //
        //////////////////////////////
        // in this section we tell the RTI of all the data we are going to
        // produce, and all the data we want to know about
        publishAndSubscribe();
        log( "Published and Subscribed" );

        /////////////////////////////////////
        // 8. register objects to update //
        /////////////////////////////////////
        int[] avatar_objectHandle = avatar_registerObject(auto_avatar_number);
        int[] house_objectHandle = house_registerObject();
        int[] player_objectHandle = avatar_registerObject(player_number);
        log( "Registered done!");

        // Initialize original values
        SetOriginalValues(avatar_objectHandle, player_objectHandle, house_objectHandle);
        
        // before the main loop, publish initialized data to all players
        advanceTime(0.5);
        // update the attribute values
 	updateAttributeValues();
        // send out attribute values
        SendAttributeValues(avatar_objectHandle, player_objectHandle, house_objectHandle);
        advanceTime(0.5);
        ////////////////////////////////////
        // 9. do the main simulation loop //
        ////////////////////////////////////
        // here is where we do the meat of our work. in each iteration, we will
        // update the attribute values of the object we registered, and will
        // send an interaction.
        do {
            // 9.1 Auto avatar move
            for (int i=0; i<auto_avatar_number; i++){
                auto_avatars[i].move();
            }
            
            // 9.2 Check house replenishment
            for(int i=0;i<house_number;i++){
        	houses[i].checkReplenishment();
            }
            
            // 9.3 update the attribute values
            updateAttributeValues();
            
            // 9.4 Send the updated attribute values
            SendAttributeValues(avatar_objectHandle, player_objectHandle, house_objectHandle);

            // 9.5 request a time advance and wait until we get it
            advanceTime( 1.0 );
            log("Online players: "+ online_players + " Time Advanced to " + fedamb.federateTime);
            
        }while(online_players > 0);

        //////////////////////////////////////
        // 10. delete the object we created //
        //////////////////////////////////////
        deleteObject(avatar_objectHandle, auto_avatar_number );
	log( "Deleted Auto Avatar Object");
        
        deleteObject(player_objectHandle, player_number );
	log( "Deleted Player Object");

	deleteObject(house_objectHandle, house_number );
        log( "Deleted House Object");

        ////////////////////////////////////
        // 11. resign from the federation //
        ////////////////////////////////////
        rtiamb.resignFederationExecution( ResignAction.NO_ACTION );
        log( "Resigned from Federation" );

        ////////////////////////////////////////
        // 12. try and destroy the federation //
        ////////////////////////////////////////
        // NOTE: we won't die if we can't do this because other federates
        //       remain. in that case we'll leave it for them to clean up
        try
        {
            rtiamb.destroyFederationExecution( "EnvironmentFederation" );
            log( "Destroyed Federation" );
        }
        catch( FederationExecutionDoesNotExist dne )
        {
            log( "No need to destroy federation, it doesn't exist" );
        }
        catch( FederatesCurrentlyJoined fcj )
        {
            log( "Didn't destroy federation, federates still joined" );
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////// Helper Methods //////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * This method will attempt to enable the various time related properties for
     * the federate
     */
    private void enableTimePolicy() throws RTIexception
    {
        // NOTE: Unfortunately, the LogicalTime/LogicalTimeInterval create code is
        //       Portico specific. You will have to alter this if you move to a
        //       different RTI implementation. As such, we've isolated it into a
        //       method so that any change only needs to happen in a couple of spots 
        LogicalTime currentTime = convertTime( fedamb.federateTime );
        LogicalTimeInterval lookahead = convertInterval( fedamb.federateLookahead );

        ////////////////////////////
        // enable time regulation //
        ////////////////////////////
        this.rtiamb.enableTimeRegulation( currentTime, lookahead );

        // tick until we get the callback
        while( fedamb.isRegulating == false )
        {
            rtiamb.tick();
        }

        /////////////////////////////
        // enable time constrained //
        /////////////////////////////
        this.rtiamb.enableTimeConstrained();

        // tick until we get the callback
        while( fedamb.isConstrained == false )
        {
            rtiamb.tick();
        }
    }

    /**
     * This method will inform the RTI about the types of data that the federate will
     * be creating, and the types of data we are interested in hearing about as other
     * federates produce it.
     */
    private void publishAndSubscribe() throws RTIexception
    {
        // Get all the handle information for the attributes of ObjectRoot.Avatar
        avatar_handle = rtiamb.getObjectClassHandle( "ObjectRoot.Avatar" );
        int avatar_x = rtiamb.getAttributeHandle( "x", avatar_handle );
        int avatar_y = rtiamb.getAttributeHandle( "y", avatar_handle );
        int avatar_candy_number = rtiamb.getAttributeHandle( "candy_number", avatar_handle );		
        int avatar_name = rtiamb.getAttributeHandle( "entity_name", avatar_handle );

        // Package the information into a handle set
        AttributeHandleSet attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(avatar_x);
        attributes.add(avatar_y);
        attributes.add(avatar_candy_number);
        attributes.add(avatar_name);

        // Do the actual publication and subscribe
        rtiamb.publishObjectClass( avatar_handle, attributes );
        rtiamb.subscribeObjectClassAttributes( avatar_handle, attributes );

        // Get all the handle information for the attributes of ObjectRoot.House
        house_handle = rtiamb.getObjectClassHandle( "ObjectRoot.House" );
        int house_x = rtiamb.getAttributeHandle( "x", house_handle );
        int house_y = rtiamb.getAttributeHandle( "y", house_handle );
        int house_candy_number = rtiamb.getAttributeHandle( "candy_number", house_handle );
        int house_name = rtiamb.getAttributeHandle( "entity_name", house_handle );

        AttributeHandleSet house_attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        house_attributes.add(house_x);
        house_attributes.add(house_y);
        house_attributes.add(house_candy_number);
        house_attributes.add(house_name);

        // Only publish house information
        rtiamb.publishObjectClass( house_handle, house_attributes );
    }

    // Register avtar objects
    private int[] avatar_registerObject(int number) throws RTIexception
    {
        int[] ObjectHandle = new int[number];
        for (int i=0; i<number; i++)
        {
            int classHandle = rtiamb.getObjectClassHandle( "ObjectRoot.Avatar" );
            ObjectHandle[i] = rtiamb.registerObjectInstance(classHandle);
        } 
        return ObjectHandle;
    }

    // Register house objects
    private int[] house_registerObject() throws RTIexception
    {
        int[] ObjectHandle = new int[house_number];
        for (int i=0; i<house_number; i++)
        {
            int classHandle = rtiamb.getObjectClassHandle( "ObjectRoot.House" );
            ObjectHandle[i] = rtiamb.registerObjectInstance(classHandle);
        } 
        return ObjectHandle;
    }
    
    // Initialize original values for all objects
    private void SetOriginalValues(int[] avatar_objectHandle, int[] player_objectHandle, int[] house_objectHandle) throws RTIexception{
        for(int i=0; i < auto_avatar_number; i++)
            updateAttributeValues(auto_avatars[i],avatar_objectHandle[i]);
        
        for(int i=0; i < house_number; i++)
            updateAttributeValues(houses[i],house_objectHandle[i]);
        
        for(int i=0; i < player_number; i++)
            updateAttributeValues(player_avatars[i],player_objectHandle[i]);
    }


    private void updateAttributeValues() throws RTIexception
    {
        // Detect collision for each auto avatar with houses or other auto avatars
        for(int i=0; i < auto_avatar_number; i++)
        {
            // Detect collosion with auto avatars
            for(int j=i+1; j < auto_avatar_number; j++){
                if (CheckCollision(auto_avatars[i], auto_avatars[j])){
                    auto_avatars[i].candy_number = auto_avatars[j].getCandy(auto_avatars[i].candy_number);
                }
            }
            // Detect collosion with houses
            for (int j=0; j < house_number; j++){
                if (CheckCollision(auto_avatars[i], houses[j])){
                    auto_avatars[i].candy_number = auto_avatars[i].candy_number + houses[j].getCandy();
                }
            }
        }
        
        // Detect collision among player with houses or auto avatars or other players
        for(int i=0; i < player_number; i++){
            // Detect collosion with auto avatars
            for(int j=0; j < auto_avatar_number; j++){
                if (CheckCollision(player_avatars[i], auto_avatars[j])){
                    auto_avatars[j].candy_number = player_avatars[i].getCandy(auto_avatars[j].candy_number);
                    auto_avatars[j].IsVisible = true;
                }
                else if (player_avatars[i].getDistance(auto_avatars[j]) <= AVATAR_VISION_RANGE){
                    auto_avatars[j].IsVisible = true;
                }
            }
            
            // Detect collosion with houses
            for (int j=0; j < house_number; j++){
                if (CheckCollision(player_avatars[i], houses[j])){
                    player_avatars[i].candy_number = player_avatars[i].candy_number + houses[j].getCandy();
                    houses[j].IsVisible = true;
                }
                else if (player_avatars[i].getDistance(houses[j]) <= AVATAR_VISION_RANGE){
                    houses[j].IsVisible = true;
                }
            }

            // Detect collosion with pother players
            for(int j=i+1; j < player_number; j++){
                if (CheckCollision(player_avatars[i], player_avatars[j])){
                    player_avatars[j].candy_number = player_avatars[i].getCandy(player_avatars[j].candy_number);
                    player_avatars[j].IsVisible = true;
                }
                else if (player_avatars[i].getDistance(player_avatars[j]) <= AVATAR_VISION_RANGE){
                    player_avatars[j].IsVisible = true;
                }
            }
        }
        log( "All data updated at this time!" );
    }
    
    private void SendAttributeValues(int[] avatar_objectHandle, int[] player_objectHandle, int[] house_objectHandle) throws RTIexception{
        for(int i=0; i < auto_avatar_number; i++){
            if (auto_avatars[i].IsVisible){
                updateAttributeValues(auto_avatars[i],avatar_objectHandle[i]);
                auto_avatars[i].IsVisible = false;
            }
        }
        
        for(int i=0; i < house_number; i++){
            if (houses[i].IsVisible){
                updateAttributeValues(houses[i],house_objectHandle[i]);
                houses[i].IsVisible = false;
            }
        }
        
        for(int i=0; i < player_number; i++){
            updateAttributeValues(player_avatars[i],player_objectHandle[i]);
        }
    }
    
    /**
     * This method will update all the values of the given object instance. It will
     * set each of the values to be a string which is equal to the name of the
     * attribute plus the current time. eg "aa:10.0" if the time is 10.0.
     * <p/>
     * Note that we don't actually have to update all the attributes at once, we
     * could update them individually, in groups or not at all!
     */
    private void updateAttributeValues(Entity e, int objectHandle) throws RTIexception{
        SuppliedAttributes attributes = RtiFactoryFactory.getRtiFactory().createSuppliedAttributes();

        byte[]  xValue = EncodingHelpers.encodeDouble(e.x );
        byte[]  yValue = EncodingHelpers.encodeDouble(e.y );
        byte[] CandyValue = EncodingHelpers.encodeInt(e.candy_number);
        byte[] NameValue = EncodingHelpers.encodeString(e.entity_name);

        int classHandle = rtiamb.getObjectClass(objectHandle);
        int x_Handle = rtiamb.getAttributeHandle( "x", classHandle );
        int y_Handle = rtiamb.getAttributeHandle( "y", classHandle );
        int candy_Handle= rtiamb.getAttributeHandle( "candy_number", classHandle );
        int Name_Handle= rtiamb.getAttributeHandle( "entity_name", classHandle );

        // put the values into the collection
        attributes.add( x_Handle, xValue );
        attributes.add( y_Handle, yValue );
        attributes.add( candy_Handle, CandyValue );
        attributes.add( Name_Handle, NameValue );

        LogicalTime time = convertTime( fedamb.federateTime + fedamb.federateLookahead );
        rtiamb.updateAttributeValues(objectHandle, attributes, generateTag(), time );
    }

    // Methed to check collisions
    private boolean CheckCollision(Entity e1, Entity e2){
        double dist_x = e1.x - e2.x;
        double dist_y = e1.y - e2.y;
        double dist = Math.sqrt(dist_x*dist_x + dist_y*dist_y);
        return dist < AVATAR_SENSE_RANGE;
    }
    /**
     * This method will request a time advance to the current time, plus the given
     * timestep. It will then wait until a notification of the time advance grant
     * has been received.
     */
    private void advanceTime( double timestep ) throws RTIexception
    {
        // request the advance
        fedamb.isAdvancing = true;
        LogicalTime newTime = convertTime( fedamb.federateTime + timestep );
        rtiamb.timeAdvanceRequest( newTime );

        // wait for the time advance to be granted. ticking will tell the
        // LRC to start delivering callbacks to the federate
        while( fedamb.isAdvancing )
        {
            rtiamb.tick();
        }
    }

    /**
     * This method will attempt to delete the object instance of the given
     * handle. We can only delete objects we created, or for which we own the
     * privilegeToDelete attribute.
     */
    private void deleteObject( int[] handle,int number ) throws RTIexception
    {
        for(int i=0;i<number;++i)
        {
            rtiamb.deleteObjectInstance( handle[i], generateTag() );
        }
    }

    // generate tag
    private byte[] generateTag()
    {
        String tag="Environment";
        return EncodingHelpers.encodeString(tag);
    }

    // Initialize random data for all objects
    private void Initialize(){
        // Initialize auto avatar objects
        auto_avatars = new AvatarEntity[auto_avatar_number];
        for(int i=0; i<auto_avatar_number; i++){ 
            Random rand = new Random();
            int random_x = rand.nextInt(MAP_WIDTH);
            int random_y = rand.nextInt(MAP_HEIGHT);
            auto_avatars[i] = new AvatarEntity("Auto Avatar " + i,AUTO_AVATAR_IMG, AVATAR_SENSE_RANGE, random_x, random_y);
            auto_avatars[i].setAvatarEntityProperty(AVATAR_SPEED, AVATAR_VISION_RANGE, true);
            auto_avatars[i].candy_number = 0;
            auto_avatars[i].setNewDirection();
        }
        
        // Initialize player objects
        player_avatars = new AvatarEntity[player_number];
        for(int i=0; i<player_number; i++){ 
            Random rand = new Random();
            int random_x = rand.nextInt(MAP_WIDTH);
            int random_y = rand.nextInt(MAP_HEIGHT);
            player_avatars[i] = new AvatarEntity("Player " + i, PLAYER_AVATAR_IMG, AVATAR_SENSE_RANGE, random_x, random_y);
            player_avatars[i].setAvatarEntityProperty(AVATAR_SPEED, AVATAR_VISION_RANGE, false);
            player_avatars[i].candy_number = 0;
        }
        
        // Initialize house objects
        houses = new HouseEntity[house_number];   
        for(int i=0; i<house_number; i++){
            Random rand = new Random();
            int random_x = rand.nextInt(MAP_WIDTH);
            int random_y = rand.nextInt(MAP_HEIGHT);
            Random random = new Random();
            houses[i] = new HouseEntity("House " + i, HOUSE_IMG, AVATAR_SENSE_RANGE, random_x, random_y);
            houses[i].setHouseEntityProperty(random.nextDouble(), REPLENISH_AMOUNT, random.nextInt(10) + 5);
            houses[i].candy_number = random.nextInt(600) + 400;
        }
    }

    //----------------------------------------------------------
    //                     STATIC METHODS
    //----------------------------------------------------------
    public static void main( String[] args )
    {
        // Default value
        auto_avatar_number = 200;
        house_number = 200;
        player_number = 1;
            
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter the total Auto Avatar number: ");
        auto_avatar_number = reader.nextInt();
        System.out.println("Enter the total House number: ");
        house_number = reader.nextInt();
        System.out.println("Enter the total Player number: ");
        player_number = reader.nextInt();
        
        try
        {
            // run the example federate
            new EnvironmentFederate().runFederate("HalloweenFederate");
        }
        catch( RTIexception rtie ){}
    }
}