package HalloweenTime.Game;

import HalloweenTime.Entity.HouseEntity;
import HalloweenTime.Entity.AvatarEntity;
import HalloweenTime.Entity.Entity;
import static HalloweenTime.Game.SystemSettings.AUTO_AVATAR_IMG;
import static HalloweenTime.Game.SystemSettings.AUTO_AVATAR_TAG;
import static HalloweenTime.Game.SystemSettings.AVATAR_SENSE_RANGE;
import static HalloweenTime.Game.SystemSettings.AVATAR_SPEED;
import static HalloweenTime.Game.SystemSettings.AVATAR_VISION_RANGE;
import static HalloweenTime.Game.SystemSettings.GAME_MESSAGE;
import static HalloweenTime.Game.SystemSettings.WINDOW_HEIGHT;
import static HalloweenTime.Game.SystemSettings.WINDOW_WIDTH;
import static HalloweenTime.Game.SystemSettings.PLAYER_AVATAR_IMG;
import static HalloweenTime.Game.SystemSettings.PLAYER_AVATAR_TAG;
import static HalloweenTime.Game.SystemSettings.START_MESSAGE;
import static HalloweenTime.Game.SystemSettings.COLOR_BACKGROUND;
import static HalloweenTime.Game.SystemSettings.COLOR_TEXT;
import static HalloweenTime.Game.SystemSettings.HOUSE_IMG;
import static HalloweenTime.Game.SystemSettings.HOUSE_TAG;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferStrategy;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * The main class based on Kevin Glass Space Invader game, 
 * it manages the display and mediates the game logic.
 * 
 * @author Richard
 *
 */
public class HalloweenGame extends Canvas 
{
    // Collect visible entities
    public Hashtable<String,Entity> VisibleEntities = new Hashtable<String,Entity>();
    // True if the game is currently "running", i.e. the game loop is looping
    public boolean gameRunning = true;
    // The strategy that allows us to use accelerate page flipping
    private final BufferStrategy strategy;
    // Player's controlled avatar 
    private static AvatarEntity myAvatar;

    //////////////////////////
    // KEYBOARD HANDLING
    //////////////////////////
    // True if we're holding up game play until a key has been pressed
    private boolean waitingForKeyPress = true;
    private boolean leftPressed = false;    // True if the left cursor key is currently pressed 
    private boolean rightPressed = false;   // True if the right cursor key is currently pressed
    private boolean upPressed = false;      // True if the up cursor key is currently pressed
    private boolean downPressed = false;    // True if the down cursor key is currently pressed

    public HalloweenGame() 
    {
        // Initialize game frame
        CreateGameFrames();
        // add key input system
        addKeyListener(new KeyInputHandler());
        requestFocus(); //focus key event to game frame

        this.createBufferStrategy(2);
        strategy = this.getBufferStrategy();

        //initialize first game
        waitingForKeyPress = true;
        gameRunning = true;
    }
    
    private void CreateGameFrames(){
        // Create a frame to contain the game. 
        JFrame container = new JFrame("Halloween Time");

        // Configurate the frame's panel: resolutions, bounds, repain mode
        JPanel panel = (JPanel) container.getContentPane();
        panel.setPreferredSize(new Dimension(WINDOW_WIDTH,WINDOW_HEIGHT));
        panel.setLayout(null);
        setBounds(0,0,WINDOW_WIDTH,WINDOW_HEIGHT);
        panel.add(this);
        setIgnoreRepaint(true);

        // Make the window visible
        container.pack();
        container.setResizable(true);
        container.setVisible(true);

        // Add listeners to response to user's events
        // Exit when user closes the window
        container.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                gameRunning = false;
                System.exit(0);
            }
        });
    }
	
    // Set game's control avatar
    public void setMyAvatar(AvatarEntity avatar) {
        myAvatar = avatar;
    }

    // Create different entities
    public Entity createEntity(int tag, String name, double location_x, double location_y, int candy_number) 
    {
        Entity entity = null;
        switch (tag)
        {
        case HOUSE_TAG:
            entity = new HouseEntity(name, HOUSE_IMG, AVATAR_SENSE_RANGE,(int)location_x, (int)location_y);
            entity.candy_number = candy_number;
            break;
        case AUTO_AVATAR_TAG:
            entity = new AvatarEntity(name, AUTO_AVATAR_IMG, AVATAR_SENSE_RANGE, (int)location_x, (int)location_y);
            AvatarEntity autp_avatar_entity = (AvatarEntity)entity;
            autp_avatar_entity.setAvatarEntityProperty(AVATAR_SPEED, AVATAR_VISION_RANGE, true);
            entity.candy_number = candy_number;
            break;
        case PLAYER_AVATAR_TAG:
            entity = new AvatarEntity(name, PLAYER_AVATAR_IMG, AVATAR_SENSE_RANGE, (int)location_x, (int)location_y);
            AvatarEntity avatar_entity = (AvatarEntity)entity;
            avatar_entity.setAvatarEntityProperty(AVATAR_SPEED, AVATAR_VISION_RANGE, false);
            entity.candy_number = candy_number;
            break;
        default:
            break;
        }
        return entity;
    }

    // Start a fresh game
    public void startGame() 
    {
        leftPressed = false;
        rightPressed = false;
        upPressed = false;
        downPressed = false;
    }
    
    // Game body
    public void gameBody() 
    {
        //-------------------------------------------
        // 1. REFRESH THE SCREEN
        //-------------------------------------------
        // refresh the screen
        Graphics2D g = (Graphics2D) strategy.getDrawGraphics();
        g.setColor(Color.GRAY);
        g.fillRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        // move / update location of player entity
        if (!waitingForKeyPress) {
            myAvatar.move();
        }

        // draw the entities
        g.setColor(COLOR_BACKGROUND);
        g.fillOval(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
        Enumeration<Entity> e = VisibleEntities.elements();
        while (e.hasMoreElements()){
            Entity entity = e.nextElement();
            int related_x = (int)(myAvatar.x + WINDOW_WIDTH/2 - entity.x);
            int related_y = (int)(myAvatar.y + WINDOW_HEIGHT/2 - entity.y);
            if (related_x >= 0 && related_x <= WINDOW_WIDTH &&
                related_y >= 0 && related_y <= WINDOW_HEIGHT &&
                myAvatar.getDistance(entity) < (int)AVATAR_VISION_RANGE) {
                entity.draw(g, related_x, related_y);
            }
            else VisibleEntities.remove(entity.entity_name);
        }

        //-------------------------------------------
        //2. DISPLAY ANY NOTIFICATION
        //-------------------------------------------
        if (waitingForKeyPress) {
            //Start game event
            myAvatar.draw(g, WINDOW_WIDTH/2, WINDOW_HEIGHT/2);
            g.setColor(COLOR_TEXT);
            g.drawString(GAME_MESSAGE, (WINDOW_WIDTH-g.getFontMetrics().stringWidth(GAME_MESSAGE))/2, 350);
            g.drawString(START_MESSAGE, (WINDOW_WIDTH-g.getFontMetrics().stringWidth(START_MESSAGE))/2, 400);
        }	

        //--------------------------------------------
        //3. CLEAR UP GRAPHIC AND FLIP THE BUFFER OVER
        //--------------------------------------------
        g.dispose();
        strategy.show();	

        //-------------------------------------------
        //4. HANDLE GAME ACTION
        //-------------------------------------------
        if (myAvatar == null) return;
        if (leftPressed && !rightPressed) {
            myAvatar.setHorizontalMovement(1);
        }
        else if (rightPressed && !leftPressed) {
            myAvatar.setHorizontalMovement(-1);
        }
        else {
            myAvatar.setHorizontalMovement(0);
        }

        if (upPressed && !downPressed) {
            myAvatar.setVerticalMovement(1);
        }
        else if (downPressed && !upPressed) {
            myAvatar.setVerticalMovement(-1);
        }
        else {
            myAvatar.setVerticalMovement(0);
        }		
    }
    
    // Exit the game
    public void exitMessage()
    {
        Graphics2D g = (Graphics2D) strategy.getDrawGraphics();
        g.setColor(Color.GRAY);
        g.fillRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

        g.setColor(COLOR_BACKGROUND);
        g.fillOval(0, 0, (int)AVATAR_VISION_RANGE*2, (int)AVATAR_VISION_RANGE*2);

        g.setColor(COLOR_TEXT);
        String str = "YOU HAVE PRESSED ESC - GAME STOPPED";
        g.drawString(str, (WINDOW_WIDTH-g.getFontMetrics().stringWidth(str))/2, 350);
        str = "My Avatar Result = "+myAvatar;
        g.drawString(str, (WINDOW_WIDTH-g.getFontMetrics().stringWidth(str))/2, 400);
        myAvatar.draw(g, WINDOW_WIDTH/2, WINDOW_HEIGHT/2);
        g.dispose();
        strategy.show();
    }

    //----------------------------------------------------
    // PRIVATE METHODS
    //----------------------------------------------------

    /**
     * a class to handle keyboard input from the user. The class handles both dynamic
     * input during game play, i.e. left/right and shoot, and more static type input
     * 
     * @author Richard Huynh, adapted from Kevin Glass
     */
    private class KeyInputHandler extends KeyAdapter 
    {
        private int pressCount = 1;
        /**
         * Receive notification from AWT of a key(s) being pressed (but *NOT* released)
         * @param e
         */
        @Override
        public void keyPressed(KeyEvent e)
        {
            // If anykey will do (for continue)
            if (waitingForKeyPress) {
                return;
            }

            //direction
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                leftPressed = true;
            }
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                rightPressed = true;
            }
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                upPressed = true;
            }
            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                downPressed = true;
            }
            // if we hit escape, then quit the game
            if (e.getKeyCode()==KeyEvent.VK_ESCAPE) {
                gameRunning =false;
            }
        }

        /**
         * Receive notification from AWT of a key(s) being released
         * @param e
         */
        @Override
        public void keyReleased(KeyEvent e)
        {
            // If anykey will do (for continue)
            if (waitingForKeyPress) {
                return;
            }

            //direction
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                leftPressed = false;
            }
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                rightPressed = false;
            }
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                upPressed = false;
            }
            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                downPressed = false;
            }
        }
        /**
         * Receive notification from AWT of a key being typed (press and release)
         */
        @Override
        public void keyTyped(KeyEvent e) 
        {
            //If waiting for anykey to start the game 
            if (waitingForKeyPress) 
            {
                if (pressCount == 1) {
                    waitingForKeyPress = false;
                    pressCount = 0;
                }
                else {
                    pressCount++;
                }
            }

            // if we hit escape, then quit the game
            if (e.getKeyCode()==KeyEvent.VK_ESCAPE) {
                gameRunning =false;
            }
        }
    }//end of class KeyHandler
}
