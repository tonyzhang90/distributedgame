package HalloweenTime.Game;

import java.awt.Color;

public class SystemSettings {
    /////////////////////////////////////////////////////
    // SIMULATION CONSTANTS
    /////////////////////////////////////////////////////
    public static final String 	FEDERATION_NAME = "Game.fed";
    public static final int	HOUSE_TAG = 0;
    public static final int	AUTO_AVATAR_TAG = 1;
    public static final int	PLAYER_AVATAR_TAG = 2;

    /////////////////////////////////////////////////////
    // GAME CONSTANTS
    /////////////////////////////////////////////////////
    public static final int FRAME_TIME = 40;
    public static final int FRAME_PER_SECOND = 1000/FRAME_TIME;
    public static final int REPLENISH_AMOUNT = 200; 

    public static final double AVATAR_SPEED = 1.4;
    public static final double AVATAR_SENSE_RANGE = 30.0;
    public static final double AVATAR_VISION_RANGE = 300.0;

    /////////////////////////////////////////////////////
    // WINDOW CONSTANTS
    /////////////////////////////////////////////////////
    public static final int MAP_WIDTH = 2000;
    public static final int MAP_HEIGHT = 1500;

    public static final int WINDOW_WIDTH  = (int)AVATAR_VISION_RANGE*2;
    public static final int WINDOW_HEIGHT = (int)AVATAR_VISION_RANGE*2;

    public static final Color COLOR_BACKGROUND = Color.BLACK;
    public static final Color COLOR_TEXT = Color.WHITE;

    public static final String GAME_MESSAGE = "CE7490 Distributed Game: Halloween Time";
    public static final String START_MESSAGE = "Press any key to start";

    //////////////////////////////////////////////////////
    // ICON LOCATIONS
    //////////////////////////////////////////////////////
    public static final String HOUSE_IMG = "Icons/house.png";
    public static final String PLAYER_AVATAR_IMG = "Icons/user.png";
    public static final String AUTO_AVATAR_IMG = "Icons/npc.png";
}
