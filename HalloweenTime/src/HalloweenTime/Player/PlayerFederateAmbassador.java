/*
 *   Copyright 2007 The Portico Project
 *
 *   This file is part of portico.
 *
 *   portico is free software; you can redistribute it and/or modify
 *   it under the terms of the Common Developer and Distribution License (CDDL) 
 *   as published by Sun Microsystems. For more information see the LICENSE file.
 *   
 *   Use of this software is strictly AT YOUR OWN RISK!!!
 *   If something bad happens you do not have permission to come crying to me.
 *   (that goes for your lawyer as well)
 *
 */
package HalloweenTime.Player;

import HalloweenTime.Entity.AvatarEntity;
import HalloweenTime.Entity.Entity;
import static HalloweenTime.Game.SystemSettings.AUTO_AVATAR_TAG;
import static HalloweenTime.Game.SystemSettings.HOUSE_TAG;
import static HalloweenTime.Game.SystemSettings.PLAYER_AVATAR_TAG;
import hla.rti.EventRetractionHandle;
import hla.rti.LogicalTime;
import hla.rti.RTIexception;
import hla.rti.ReflectedAttributes;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.NullFederateAmbassador;

import org.portico.impl.hla13.types.DoubleTime;

/**
 * This class handles all incoming callbacks from the RTI regarding a particular
 * {@link PlayerFederate}. It will log information about any callbacks it
 * receives, thus demonstrating how to deal with the provided callback information.
 */
public class PlayerFederateAmbassador extends NullFederateAmbassador
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	// these variables are accessible in the package
	protected double federateTime        = 0.0;
	protected double federateLookahead   = 1.0;
	
	protected boolean isRegulating       = false;
	protected boolean isConstrained      = false;
	protected boolean isAdvancing        = false;
	
	protected boolean isAnnounced        = false;
	protected boolean isReadyToRun       = false;

        protected PlayerFederate Federate;
	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------

	public PlayerFederateAmbassador(PlayerFederate Federate)
	{
            this.Federate = Federate;
	}

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------
	/**
	 * As all time-related code is Portico-specific, we have isolated it into a single
	 * method. This way, if you need to move to a different RTI, you only need to
	 * change this code, rather than more code throughout the whole class.
	 */
	private double convertTime( LogicalTime logicalTime )
	{
		// PORTICO SPECIFIC!!
		return ((DoubleTime)logicalTime).getTime();
	}
	
	private void log( String message )
	{
		System.out.println( "FederateAmbassador: " + message );
	}
	
	//////////////////////////////////////////////////////////////////////////
	////////////////////////// RTI Callback Methods //////////////////////////
	//////////////////////////////////////////////////////////////////////////
        @Override
	public void synchronizationPointRegistrationFailed( String label )
	{
		log( "Failed to register sync point: " + label );
	}

        @Override
	public void synchronizationPointRegistrationSucceeded( String label )
	{
		log( "Successfully registered sync point: " + label );
	}

        @Override
	public void announceSynchronizationPoint( String label, byte[] tag )
	{
		log( "Synchronization point announced: " + label );
		if( label.equals(PlayerFederate.READY_TO_RUN) )
			this.isAnnounced = true;
	}

        @Override
	public void federationSynchronized( String label )
	{
		log( "Federation Synchronized: " + label );
		if( label.equals(PlayerFederate.READY_TO_RUN) )
			this.isReadyToRun = true;
	}

	/**
	 * The RTI has informed us that time regulation is now enabled.
	 */
        @Override
	public void timeRegulationEnabled( LogicalTime theFederateTime )
	{
		this.federateTime = convertTime( theFederateTime );
		this.isRegulating = true;
	}

        @Override
	public void timeConstrainedEnabled( LogicalTime theFederateTime )
	{
		this.federateTime = convertTime( theFederateTime );
		this.isConstrained = true;
	}

        @Override
	public void timeAdvanceGrant( LogicalTime theTime )
	{
		this.federateTime = convertTime( theTime );
		this.isAdvancing = false;
	}

        @Override
	public void discoverObjectInstance( int theObject,
	                                    int theObjectClass,
	                                    String objectName )
	{
		log( "Discoverd Object: handle=" + theObject + ", classHandle=" +
		     theObjectClass + ", name=" + objectName );
	}

        @Override
	public void reflectAttributeValues( int theObject,
	                                    ReflectedAttributes theAttributes,
	                                    byte[] tag )
	{
		// just pass it on to the other method for printing purposes
		// passing null as the time will let the other method know it
		// it from us, not from the RTI
		reflectAttributeValues( theObject, theAttributes, tag, null, null );
	}

        @Override
	public void reflectAttributeValues( int theObject,
	                                    ReflectedAttributes theAttributes,
	                                    byte[] tag,
	                                    LogicalTime theTime,
	                                    EventRetractionHandle retractionHandle )
	{
            try{
                String src= EncodingHelpers.decodeString(tag);
                if(!src.equals("Environment")) return;
                
                double X = 0.0; double Y =0.0; int candynumber = 0; String name = "";           
                int Class_Handle = Federate.rtiamb.getObjectClass(theObject);
                if(Class_Handle == Federate.avatar_handle){
                    int x = Federate.rtiamb.getAttributeHandle( "x", Class_Handle);
                    int y = Federate.rtiamb.getAttributeHandle( "y", Class_Handle);
                    int candy_number = Federate.rtiamb.getAttributeHandle( "candy_number", Class_Handle);
                    int entity_name = Federate.rtiamb.getAttributeHandle( "entity_name", Class_Handle);
                    
                    // get Attributes value
                    for(int i=0;i<theAttributes.size();i++)
                    {
                        if(theAttributes.getAttributeHandle(i)==x)
                        {
                            X = EncodingHelpers.decodeDouble(theAttributes.getValue(i));
                        }
                        else if(theAttributes.getAttributeHandle(i)==y)
                        {
                            Y = EncodingHelpers.decodeDouble(theAttributes.getValue(i));
                        }
                        else if(theAttributes.getAttributeHandle(i)==candy_number)
                        {
                            candynumber = EncodingHelpers.decodeInt(theAttributes.getValue(i));
                        }
                        else if(theAttributes.getAttributeHandle(i)==entity_name)
                        {
                            name = EncodingHelpers.decodeString(theAttributes.getValue(i));
                        }
                    }
                    log("Receive avatar update: " + name + " (" + X + "|" + Y + "|" + candynumber + ")");
                    Entity entity = Federate.newGame.VisibleEntities.get(name);
                    if (entity == null){ // new entity
                        // Check the data is auto avatar or player
                        if (name.contains("Player")){
                            // Check the data is self or other player
                            Federate.newGame.VisibleEntities.put(name, Federate.newGame.createEntity(PLAYER_AVATAR_TAG, name, X, Y, candynumber));
                            if (name.equals(Federate.PlayerAvatar.entity_name)){
                                Federate.PlayerAvatar = (AvatarEntity)entity;
				Federate.newGame.setMyAvatar(Federate.PlayerAvatar);
                            }
                        }
                        else Federate.newGame.VisibleEntities.put(name, Federate.newGame.createEntity(AUTO_AVATAR_TAG, name, X, Y, candynumber));
                    }
                    else{ // existed entity
                        // for other players, auto avatars or houses, update location and candy number 
                        entity.x = X;
                        entity.y = Y;
                        entity.candy_number = candynumber;
                    }
                }
                else if (Class_Handle == Federate.house_handle){
                    int x = Federate.rtiamb.getAttributeHandle( "x", Class_Handle);
                    int y = Federate.rtiamb.getAttributeHandle( "y", Class_Handle);
                    int candy_number = Federate.rtiamb.getAttributeHandle( "candy_number", Class_Handle);
                    int entity_name = Federate.rtiamb.getAttributeHandle( "entity_name", Class_Handle);
                    
                    // get Attributes value
                    for(int i=0;i<theAttributes.size();i++)
                    {
                        if(theAttributes.getAttributeHandle(i)==x)
                        {
                            X = EncodingHelpers.decodeDouble(theAttributes.getValue(i));
                        }
                        else if(theAttributes.getAttributeHandle(i)==y)
                        {
                            Y = EncodingHelpers.decodeDouble(theAttributes.getValue(i));
                        }
                        else if(theAttributes.getAttributeHandle(i)==candy_number)
                        {
                            candynumber = EncodingHelpers.decodeInt(theAttributes.getValue(i));
                        }
                        else if(theAttributes.getAttributeHandle(i)==entity_name)
                        {
                            name = EncodingHelpers.decodeString(theAttributes.getValue(i));
                        }
                    }
                    
                    log("Receive house update: " + name + " (" + X + "|" + Y + "|" + candynumber + ")");
                    Entity entity = Federate.newGame.VisibleEntities.get(name);
                    if (entity == null){
                        Federate.newGame.VisibleEntities.put(name, Federate.newGame.createEntity(HOUSE_TAG, name, X, Y, candynumber));
                    }
                    else{
                        entity.candy_number = candynumber;
                    }
                }    
            }catch (RTIexception e){
                
            }
	}

        @Override
	public void removeObjectInstance( int theObject, byte[] userSuppliedTag )
	{
		log( "Object Removed: handle=" + theObject );
	}

        @Override
	public void removeObjectInstance( int theObject,
	                                  byte[] userSuppliedTag,
	                                  LogicalTime theTime,
	                                  EventRetractionHandle retractionHandle )
	{
		log( "Object Removed: handle=" + theObject );
	}


	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
}
