/*
 *   Copyright 2007 The Portico Project
 *
 *   This file is part of portico.
 *
 *   portico is free software; you can redistribute it and/or modify
 *   it under the terms of the Common Developer and Distribution License (CDDL) 
 *   as published by Sun Microsystems. For more information see the LICENSE file.
 *   
 *   Use of this software is strictly AT YOUR OWN RISK!!!
 *   If something bad happens you do not have permission to come crying to me.
 *   (that goes for your lawyer as well)
 *
 */
package HalloweenTime.Player;

import HalloweenTime.Game.HalloweenGame;
import HalloweenTime.Entity.AvatarEntity;
import static HalloweenTime.Game.SystemSettings.AVATAR_VISION_RANGE;
import static HalloweenTime.Game.SystemSettings.FRAME_TIME;
import static HalloweenTime.Game.SystemSettings.PLAYER_AVATAR_TAG;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import hla.rti.AttributeHandleSet;
import hla.rti.LogicalTime;
import hla.rti.LogicalTimeInterval;
import hla.rti.RTIambassador;
import hla.rti.RTIexception;
import hla.rti.ResignAction;
import hla.rti.SuppliedAttributes;
import hla.rti.jlc.EncodingHelpers;
import hla.rti.jlc.RtiFactoryFactory;
import java.util.Scanner;

import org.portico.impl.hla13.types.DoubleTime;
import org.portico.impl.hla13.types.DoubleTimeInterval;

public class PlayerFederate
{
    //----------------------------------------------------------
    //                    STATIC VARIABLES
    //----------------------------------------------------------

    /** The sync point all federates will sync up on before starting */
    public static final String READY_TO_RUN = "ReadyToRun";

    //----------------------------------------------------------
    //                   INSTANCE VARIABLES
    //----------------------------------------------------------
    RTIambassador rtiamb;
    private PlayerFederateAmbassador fedamb;

    // avatar and house object handle
    protected int avatar_handle;
    protected int house_handle;

    // Create player Avatar entity
    public AvatarEntity PlayerAvatar;
    // Define the game variable
    public final HalloweenGame newGame;
    // player name
    private final String playerName;
    

    //----------------------------------------------------------
    //                      CONSTRUCTORS
    //----------------------------------------------------------

    public PlayerFederate(int id){
        playerName = "Player " + id;
        newGame = new HalloweenGame(); // create new game object
        newGame.startGame(); // start the game
        // create player Avatar entity
        PlayerAvatar = (AvatarEntity)newGame.createEntity(PLAYER_AVATAR_TAG, "dummy", AVATAR_VISION_RANGE, AVATAR_VISION_RANGE, 0);
        newGame.setMyAvatar(PlayerAvatar);
        newGame.gameBody(); // Show the start view
    }

    //----------------------------------------------------------
    //                    INSTANCE METHODS
    //----------------------------------------------------------
    /**
     * This is just a helper method to make sure all logging it output in the same form
     */
    private void log( String message )
    {
        System.out.println( "PlayerFederate : " + message );
    }

    /**
     * This method will block until the user presses enter
     */
    private void waitForUser()
    {
        log( " >>>>>>>>>> Press Enter to Continue <<<<<<<<<<" );
        BufferedReader reader = new BufferedReader( new InputStreamReader(System.in) );
        try
        {
            reader.readLine();
        }
        catch( Exception e )
        {
            log( "Error while waiting for user input: " + e.getMessage() );
        }
    }

    /**
     * As all time-related code is Portico-specific, we have isolated it into a 
     * single method. This way, if you need to move to a different RTI, you only need
     * to change this code, rather than more code throughout the whole class.
     */
    private LogicalTime convertTime( double time )
    {
        // PORTICO SPECIFIC!!
        return new DoubleTime( time );
    }

    /**
     * Same as for {@link #convertTime(double)}
     */
    private LogicalTimeInterval convertInterval( double time )
    {
        // PORTICO SPECIFIC!!
        return new DoubleTimeInterval( time );
    }

    ///////////////////////////////////////////////////////////////////////////
    ////////////////////////// Main Simulation Method /////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    public void runFederate( String federateName ) throws RTIexception
    {
        /////////////////////////////////
        // 1. create the RTIambassador //
        /////////////////////////////////
        rtiamb = RtiFactoryFactory.getRtiFactory().createRtiAmbassador();

        ////////////////////////////
        // 2. join the federation //
        ////////////////////////////
        // create the federate ambassador and join the federation
        fedamb = new PlayerFederateAmbassador(this);
        rtiamb.joinFederationExecution( federateName, "EnvironmentFederation", fedamb );
        log( "Joined Federation as " + federateName );

        ////////////////////////////////
        // 3. announce the sync point //
        ////////////////////////////////
        // announce a sync point to get everyone on the same page. if the point
        // has already been registered, we'll get a callback saying it failed,
        // but we don't care about that, as long as someone registered it
        rtiamb.registerFederationSynchronizationPoint( READY_TO_RUN, null );
        // wait until the point is announced
        while( fedamb.isAnnounced == false )
        {
            rtiamb.tick();
        }

        // WAIT FOR USER TO KICK US OFF
        // So that there is time to add other federates, we will wait until the
        // user hits enter before proceeding. That was, you have time to start
        // other federates.
        waitForUser();

        ///////////////////////////////////////////////////////
        // 4. achieve the point and wait for synchronization //
        ///////////////////////////////////////////////////////
        // tell the RTI we are ready to move past the sync point and then wait
        // until the federation has synchronized on
        rtiamb.synchronizationPointAchieved( READY_TO_RUN );
        log( "Achieved sync point: " +READY_TO_RUN+ ", waiting for federation..." );
        while( fedamb.isReadyToRun == false )
        {
            rtiamb.tick();
        }

        /////////////////////////////
        // 5. enable time policies //
        /////////////////////////////
        // in this section we enable/disable all time policies
        // note that this step is optional!
        enableTimePolicy();
        log( "Time Policy Enabled" );

        //////////////////////////////
        // 6. publish and subscribe //
        //////////////////////////////
        // in this section we tell the RTI of all the data we are going to
        // produce, and all the data we want to know about
        publishAndSubscribe();
        log( "Published and Subscribed" );

        /////////////////////////////////////
        // 7. register an object to update //
        /////////////////////////////////////
        int objectHandle = registerObject();
        log( "Registered Object, handle=" + objectHandle );
        
        //log("The original location of each avatar is allocated");
        advanceTime(0.5);

	//receive the house information from the environment
        advanceTime(0.5);

        ////////////////////////////////////
        // 8. do the main simulation loop //
        ////////////////////////////////////
        // here is where we do the meat of our work. in each iteration, we will
        // update the attribute values of the object we registered, and will
        // send an interaction.
        while(true)
        {
            long lastLoopTime = System.currentTimeMillis();
            // 9.1 update game step
            newGame.gameBody();

            // 9.2 update the attribute values of the instance //
            if (newGame.gameRunning == false) {
                // Exit game
                newGame.exitMessage();
                PlayerAvatar.x = -100.0;
                PlayerAvatar.y = -100.0;
                // update value to tell environment this player is offline
                updateAttributeValues(objectHandle);
                log("Name: "+ PlayerAvatar.entity_name+"\nCurrent location :("+ PlayerAvatar.x+" , " + PlayerAvatar.y
                +") \nCandy : "+PlayerAvatar.candy_number + "\n");
                advanceTime(0.5);
                break;
            }
            else updateAttributeValues(objectHandle);
            log("Name: "+ PlayerAvatar.entity_name+"\nCurrent location :("+ PlayerAvatar.x+" , " + PlayerAvatar.y
                +") \nCandy : "+PlayerAvatar.candy_number + "\n");

            // 9.3 Wait for the new location from the environment
            advanceTime(1.0);
            log( "Time Advanced to " + fedamb.federateTime );
            long currentLoopTime = System.currentTimeMillis();
            long remainingTime = FRAME_TIME - (currentLoopTime - lastLoopTime);
            // Syncronous steps
            try { Thread.sleep(remainingTime > 0? remainingTime:0); } catch(Exception e) {}
        }

        //////////////////////////////////////
        // 9. delete the object we created //
        //////////////////////////////////////
        deleteObject( objectHandle );
        log( "Deleted Object, handle=" + objectHandle );

        ////////////////////////////////////
        // 10. resign from the federation //
        ////////////////////////////////////
        rtiamb.resignFederationExecution( ResignAction.NO_ACTION );
        log( "Resigned from Federation" );
    }

    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////// Helper Methods //////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    /**
     * This method will attempt to enable the various time related properties for
     * the federate
     */
    private void enableTimePolicy() throws RTIexception
    {
        // NOTE: Unfortunately, the LogicalTime/LogicalTimeInterval create code is
        //       Portico specific. You will have to alter this if you move to a
        //       different RTI implementation. As such, we've isolated it into a
        //       method so that any change only needs to happen in a couple of spots 
        LogicalTime currentTime = convertTime( fedamb.federateTime );
        LogicalTimeInterval lookahead = convertInterval( fedamb.federateLookahead );

        ////////////////////////////
        // enable time regulation //
        ////////////////////////////
        this.rtiamb.enableTimeRegulation( currentTime, lookahead );

        // tick until we get the callback
        while( fedamb.isRegulating == false )
        {
            rtiamb.tick();
        }

        /////////////////////////////
        // enable time constrained //
        /////////////////////////////
        this.rtiamb.enableTimeConstrained();

        // tick until we get the callback
        while( fedamb.isConstrained == false )
        {
            rtiamb.tick();
        }
    }
	
    /**
     * This method will inform the RTI about the types of data that the federate will
     * be creating, and the types of data we are interested in hearing about as other
     * federates produce it.
     */
    private void publishAndSubscribe() throws RTIexception
    {
        // Get all the handle information for the attributes of ObjectRoot.Avatar
        avatar_handle = rtiamb.getObjectClassHandle( "ObjectRoot.Avatar" );
        int avatar_x = rtiamb.getAttributeHandle( "x", avatar_handle );
        int avatar_y = rtiamb.getAttributeHandle( "y", avatar_handle );
        int avatar_candy_number = rtiamb.getAttributeHandle( "candy_number", avatar_handle );		
        int avatar_name = rtiamb.getAttributeHandle( "entity_name", avatar_handle );

        // Package the information into a handle set
        AttributeHandleSet attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        attributes.add(avatar_x);
        attributes.add(avatar_y);
        attributes.add(avatar_candy_number);
        attributes.add(avatar_name);

        // Do the actual publication and subscribe
        rtiamb.publishObjectClass( avatar_handle, attributes );
        rtiamb.subscribeObjectClassAttributes( avatar_handle, attributes );

        // Get all the handle information for the attributes of ObjectRoot.House
        house_handle = rtiamb.getObjectClassHandle( "ObjectRoot.House" );
        int house_x = rtiamb.getAttributeHandle( "x", house_handle );
        int house_y = rtiamb.getAttributeHandle( "y", house_handle );
        int house_candy_number = rtiamb.getAttributeHandle( "candy_number", house_handle );
        int house_name = rtiamb.getAttributeHandle( "entity_name", house_handle );

        AttributeHandleSet house_attributes = RtiFactoryFactory.getRtiFactory().createAttributeHandleSet();
        house_attributes.add(house_x);
        house_attributes.add(house_y);
        house_attributes.add(house_candy_number);
        house_attributes.add(house_name);

        // Only subscribe house information
        rtiamb.subscribeObjectClassAttributes( house_handle, house_attributes );
    }
	
    /**
     * This method will register an instance of the class ObjectRoot.A and will
     * return the federation-wide unique handle for that instance. Later in the
     * simulation, we will update the attribute values for this instance
     */
    private int registerObject() throws RTIexception
    {
        int classHandle = rtiamb.getObjectClassHandle( "ObjectRoot.Avatar" );
        return rtiamb.registerObjectInstance( classHandle );
    }
	
    /**
     * This method will update all the values of the given object instance. It will
     * set each of the values to be a string which is equal to the name of the
     * attribute plus the current time. eg "aa:10.0" if the time is 10.0.
     * <p/>
     * Note that we don't actually have to update all the attributes at once, we
     * could update them individually, in groups or not at all!
     */
    private void updateAttributeValues( int objectHandle ) throws RTIexception
    {
        SuppliedAttributes attributes = RtiFactoryFactory.getRtiFactory().createSuppliedAttributes();

        byte[] xValue = EncodingHelpers.encodeDouble(PlayerAvatar.x );
        byte[] yValue = EncodingHelpers.encodeDouble(PlayerAvatar.y );
        byte[] CandyValue = EncodingHelpers.encodeInt(PlayerAvatar.candy_number);
        byte[] NameValue = EncodingHelpers.encodeString(playerName);

        int classHandle = rtiamb.getObjectClass(objectHandle);
        int x_Handle = rtiamb.getAttributeHandle( "x", classHandle );
        int y_Handle = rtiamb.getAttributeHandle( "y", classHandle );
        int candy_Handle= rtiamb.getAttributeHandle( "candy_number", classHandle );
        int Name_Handle= rtiamb.getAttributeHandle( "entity_name", classHandle );

        // put the values into the collection
        attributes.add( x_Handle, xValue );
        attributes.add( y_Handle, yValue );
        attributes.add( candy_Handle, CandyValue );
        attributes.add( Name_Handle, NameValue );

        LogicalTime time = convertTime( fedamb.federateTime + fedamb.federateLookahead );
        rtiamb.updateAttributeValues(objectHandle, attributes, generateTag(), time );
    }

    /**
     * This method will request a time advance to the current time, plus the given
     * timestep. It will then wait until a notification of the time advance grant
     * has been received.
     */
    private void advanceTime( double timestep ) throws RTIexception
    {
        // request the advance
        fedamb.isAdvancing = true;
        LogicalTime newTime = convertTime( fedamb.federateTime + timestep );
        rtiamb.timeAdvanceRequest( newTime );

        // wait for the time advance to be granted. ticking will tell the
        // LRC to start delivering callbacks to the federate
        while( fedamb.isAdvancing )
        {
            rtiamb.tick();
        }
    }

    /**
     * This method will attempt to delete the object instance of the given
     * handle. We can only delete objects we created, or for which we own the
     * privilegeToDelete attribute.
     */
    private void deleteObject( int handle ) throws RTIexception
    {
        rtiamb.deleteObjectInstance( handle, generateTag() );
    }

    private byte[] generateTag()
    {
        return EncodingHelpers.encodeString(PlayerAvatar.entity_name);
    }

    //----------------------------------------------------------
    //                     STATIC METHODS
    //----------------------------------------------------------
    public static void main( String[] args )
    {
        // get a federate name, use "exampleFederate" as default
        String federateName = "PlayerFederate";
        int id = 0;
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter your player id (e.g 0,1,2.....): ");
        id = reader.nextInt();

        try
        {
            // run the example federate
            new PlayerFederate(id).runFederate(federateName);
        }
        catch( RTIexception rtie )
        {
        }
    }
}