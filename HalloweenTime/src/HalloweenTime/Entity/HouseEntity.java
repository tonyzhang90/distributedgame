package HalloweenTime.Entity;

public class HouseEntity extends Entity {
    // The replenish counter
    protected int replenish_interval_counter;
    // The replenish interval of this house entity
    protected int replenish_interval;
    // The replenish amount of candys for this house entity
    protected int replenish_amount;
    // The amount of candys will be transmited to avatar entities
    protected double trans_persentage;
    
    public HouseEntity(String house_name, String icon_path, double sensitive_range, int x, int y) 
    {
        // Call super entity to create the house entity
        super(house_name, icon_path, sensitive_range, x, y);
    }
    
    // Set the property of this house entity
    public void setHouseEntityProperty (double trans_persentage, int replenish_amount, int replenish_interval){
        this.trans_persentage = trans_persentage;
        this.replenish_amount = replenish_amount;
        this.replenish_interval = replenish_interval;
        
        // Set the replenish counter
        replenish_interval_counter = replenish_interval;
    }

    // Check the replenish remain time and replenish candys
    public void checkReplenishment() 
    {
        if (replenish_interval_counter > 0){
            replenish_interval_counter--;
        }
        else{
            candy_number += replenish_amount;
            replenish_interval_counter = replenish_interval;
        }
    }

    // Transmit candy to avatar entities
    public int getCandy() {
        int exchange = (int)(candy_number * trans_persentage);
        candy_number -= exchange;
        return exchange;
    }
}
