package HalloweenTime.Entity;

import Icons.Helper.Icon;
import Icons.Helper.GameIconsHelper;
import static HalloweenTime.Game.SystemSettings.COLOR_TEXT;
import java.awt.Graphics;

public abstract class Entity {
    // Visible or not
    public boolean IsVisible;
    // The current x location of this entity
    public double x;
    // The current y location of this entity
    public double y;
    // The current candy number of this entity
    public int candy_number;
    // The Entity Name
    public String entity_name;
    // The sensitive range of this entity
    protected double sensitive_range;
    // The icon that represents this entity
    protected Icon icon;

    /**
     * Construct a entity based on entity name, icon and x,y location.
     * 
     * @param entity_name The name of this entity
     * @param icon_path The reference to the image to be displayed for this entity
     * @param sensitive_range The sensitive range of this entity
     * @param x The initial x location of this entity
     * @param y The initial y location of this entity
     */
    public Entity(String entity_name, String icon_path, double sensitive_range, int x, int y) {
        this.entity_name = entity_name;
        this.sensitive_range = sensitive_range;
        icon = GameIconsHelper.get().getIcon(icon_path);
        this.x = x;
        this.y = y;
        candy_number = 0;
        IsVisible = false;
    }

    //Draw this entity
    public void draw(Graphics graph, int x, int y) {
        icon.draw(graph, x, y);
        graph.setColor(COLOR_TEXT);
        graph.drawString(this.candy_number + "", x - icon.getWidth()/2, y - icon.getHeight()/2);
    }

    @Override
    public String toString() {
        return "(Name = " + entity_name + ", x = " + ((int)x) + ", y = " + ((int)y) + ", candy = " + candy_number + ")";
    }
}