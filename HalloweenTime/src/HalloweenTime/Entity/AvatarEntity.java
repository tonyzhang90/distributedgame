package HalloweenTime.Entity;

import static HalloweenTime.Game.SystemSettings.MAP_HEIGHT;
import static HalloweenTime.Game.SystemSettings.MAP_WIDTH;
import static HalloweenTime.Game.SystemSettings.FRAME_PER_SECOND;

import java.util.Random;

public class AvatarEntity extends Entity {
    // Move duration for auto avatars
    protected long duration;
    // Avatar move speed
    protected double move_speed;
    // Avatat vision range
    protected double vision_range;
    // The current speed of this entity horizontally (pixels/sec)
    protected double horizontal;
    // The current speed of this entity vertically (pixels/sec)
    protected double vertical;
    // Check this entity whether it is an auto avatar or a player avatar
    protected boolean Is_auto_avatar;

    public AvatarEntity(String avatat_name, String icon_path, double sensitive_range, int x, int y)
    {
        // Call super entity to create the avatar entity
        super(avatat_name, icon_path, sensitive_range, x, y);    
    }
    
    // Set the property of this avatar entity
    public void setAvatarEntityProperty(double move_speed, double vision_range, boolean Is_auto_avatar){
        this.move_speed	= move_speed;
        this.vision_range = vision_range;
        this.Is_auto_avatar = Is_auto_avatar;
        
        // Move property
        horizontal = 0;
        vertical = 0;
        duration = 0;
    }

    // The move action of this avatar entity
    public void move() {
        // Update location x,y based on movespeed for normal situations
        x += move_speed * horizontal;
        if(Is_auto_avatar) {
            System.out.print("horizontal:"+horizontal);
        }
        if (x < 0) {
            x = 0;
            horizontal = -horizontal;
        }
        if (x > MAP_WIDTH) {
            x = MAP_WIDTH;
            horizontal = -horizontal;
        }
        y += move_speed * vertical;
        if (y < 0) {
            y = 0;
            vertical = -vertical;
        }
        if (y > MAP_HEIGHT) {
            y = MAP_HEIGHT;
            vertical = -vertical;
        }
        
        if (Is_auto_avatar){
            duration--;
            if (duration < 0) setNewDirection();
        }
    }

    // Set the horizontal speed of this entity
    public void setHorizontalMovement(double horizontal) {
        this.horizontal = horizontal;
    }

    // Set the vertical speed of this entity
    public void setVerticalMovement(double vertical) {
        this.vertical = vertical;
    }
    
    // Calculate and return the distance between two entities
    public int getDistance(Entity e) {
        return (int)Math.sqrt((x - e.x)*(x - e.x) + (y - e.y)*(y - e.y));
    }

    // Get candys from this avatar
    public int getCandy(int otherAmount) {
        int totalnumber = candy_number + otherAmount;
        candy_number = (int)(totalnumber / 2);
        return (totalnumber - candy_number);
    }
    
    // Random move and direction, only for auto avatars
    public void setNewDirection() {
        // Random move
        Random rand = new Random();
        horizontal = rand.nextInt(3)-1;
        vertical = rand.nextInt(3)-1;
        duration = rand.nextInt(10)*FRAME_PER_SECOND;
    }
}
